#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>

#include "utils.h"

FILE *utils_read_file(char *root, int r_size, char *filename, int f_size)
{
    char file_path[MAX_PATH_SIZE];
    struct stat sb;
    FILE *f = NULL;

    memset(&file_path, 0, MAX_PATH_SIZE - 1);

    strncpy(file_path, root, r_size);
    file_path[strlen(file_path)] = '/';
    strncpy(&(file_path[strlen(file_path)]), filename, f_size);

    if (stat(file_path, &sb) == -1) {
        printf("file %s does not exist \n", file_path);
        return f;
    }
        
    printf("loading file %s \n", file_path);
    f = fopen(file_path, "r");

    if (f == NULL) {
        printf("fopen error %d \n", errno);
    }

    return f;
}
