#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

#include "inetcore.h"

int inetcore_new_socket(int type)
{
    int sfd;

    if ((sfd = socket(AF_INET, type, 0)) < 0) {
        perror("Cannot create socket \n");
        return -1;
    }

    return sfd;
}

void inetcore_new_addr(struct sockaddr_in *addr, int port)
{
    memset(addr, 0, sizeof(struct sockaddr_in));
    addr->sin_family = AF_INET;
    addr->sin_addr.s_addr = htonl(INADDR_ANY);
    addr->sin_port  = htons(port);
}

int inetcore_new_bind(int fd, struct sockaddr_in *addr)
{
    if (bind(fd, (struct sockaddr *)addr, sizeof(struct sockaddr_in)) < 0) {
        perror("Cannot bind socket \n");
        return -1;
    }

    return 0;
}
