#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "common.h"
#include "inetcore.h"
#include "tftp.h"

#define DATA_BUF_SIZE 4096

struct data_buffer {
    void *data;
    unsigned int allocated;
    unsigned int used;
};

static void grow_data_buffer(struct data_buffer *buf) 
{
    int allocate = buf->used * 2;
    void *old = buf->data;
    void *tmp = calloc(allocate, 1);

    printf("Grow data buffer - used: %d, add: %d \n", buf->used, allocate);
    buf->data = memmove(tmp, buf->data, buf->allocated);
    buf->allocated = allocate;

    free(old);
}

int main(int argc, char **argv)
{
    int sfd;
    struct sockaddr_in svr_addr;
    socklen_t addrlen = sizeof(svr_addr);
    ssize_t num_read;
    struct tftp_request_pkt req_pkt;
    struct tftp_data_pkt data_pkt;
    struct data_buffer db;

    /* we are using an fd_set because if the file bytes land on a 512 boundary the last
     * packet will be equal to a full data packet, this allows us to timeout */
    int retval;
    int retry;
    fd_set rdfs;
    struct timeval tv;

    if (argc < 2) {
        printf("Usage: %s <filename> \n", argv[0]);
        exit(EXIT_FAILURE);
    }

    /* allocate data buffer for file */
    db.allocated = DATA_BUF_SIZE;
    db.data = calloc(db.allocated, 1);
    db.used = 0;

    if ((sfd = inetcore_new_socket(SOCK_DGRAM)) < 0) {
        perror("Cannot create socket \n");
        exit(EXIT_FAILURE);
    }

    inetcore_new_addr(&svr_addr, TFTP_PORT);

    memset(&data_pkt, 0, sizeof(struct tftp_data_pkt));
    memset(&req_pkt, 0, sizeof(struct tftp_request_pkt));

    req_pkt.optcode = htons(OPTCODE_RRQ);
    strcpy(&(req_pkt.filename[0]), argv[1]);
    strcpy(&(req_pkt.mode[0]), MODE_OCTET);

    if (sendto(sfd, &req_pkt, sizeof(req_pkt), 0, (struct sockaddr *)&svr_addr, 
            addrlen) != sizeof(req_pkt)) {
        perror("Message send failed \n");
    }

    /* add sfd to fd_set */
    FD_ZERO(&rdfs);
    FD_SET(sfd, &rdfs);
    

    /* add a retry count */
    retry = 3;

    printf("Messsage sent to server \n");
    while(1) {
        struct tftp_ack_pkt ack_pkt;

        /* wait for up to 2 seconds */
        tv.tv_sec = 2;
        tv.tv_usec = 0;

        /* use select because recvfrom is blocking */
        retval = select(sfd+1, &rdfs, NULL, NULL, &tv);
        if (retval == -1) {
            printf("select error %d \n", errno);
            exit(EXIT_FAILURE);
        } else if (retval <= 0) {
            printf("timeout, retry %d \n", retry);
            retry -= 1;
            if (retry == 0) {
                printf("Done waiting on server, extiting \n");
                break;
            }
            continue;
        }

        num_read = recvfrom(sfd, &data_pkt, sizeof(struct tftp_data_pkt), 0, 
            (struct sockaddr *)&svr_addr, &addrlen);
        
        printf("Optcode %d, block %d has %d bytes, using port %d \n", 
                ntohs(data_pkt.optcode), ntohs(data_pkt.block), 
                (int)num_read, svr_addr.sin_port);

        switch (ntohs(data_pkt.optcode)) {
            case OPTCODE_DATA:
                if ((num_read - 4) > (db.allocated - db.used)) {
                    grow_data_buffer(&db);
                }
                memcpy(((char *)db.data+db.used), &(data_pkt.data), num_read - 4);
                db.used += num_read - 4;
                
                ack_pkt.optcode = htons(OPTCODE_ACK);
                /* block is already in network order, no need to swap */
                ack_pkt.block = data_pkt.block;
                
                printf("sending ack for block %d \n", ntohs(ack_pkt.block));
                if (sendto(sfd, &ack_pkt, sizeof(ack_pkt), 0, 
                        (struct sockaddr *)&svr_addr, addrlen) != sizeof(ack_pkt)) {
                    perror("Message send failed \n");
                }

                break;
            default:
                printf("optcode %d not handled \n", ntohs(data_pkt.optcode));
        }
   
        if (num_read < sizeof(struct tftp_data_pkt)) {
            break;
        }
    }

    printf("file len is %d bytes \n", db.used);
    free(db.data);

    close(sfd);
     
    return 0;
}
