#ifndef TFTP_H
#define TFTP_H

#include <stdint.h>

#define TFTP_PORT       69

#define OPTCODE_RRQ     1
#define OPTCODE_WRQ     2
#define OPTCODE_DATA    3
#define OPTCODE_ACK     4
#define OPTCODE_ERROR   5

#define MODE_OCTET "octet"

#define MAX_FILENAME_SIZE 64
#define MAX_DATA_SIZE 512

struct tftp_request_pkt {
    unsigned short optcode;
    char filename[MAX_FILENAME_SIZE];
    char pad_0;
    char mode[8];
    char pad_1;
};

struct tftp_data_pkt {
    unsigned short optcode;
    unsigned short block;
    char data[MAX_DATA_SIZE];
};

struct tftp_ack_pkt {
    unsigned short optcode;
    unsigned short block;
};
    
struct tftp_error_pkt {
    unsigned short optcode;
    unsigned short errcode;
    char errmsg[MAX_FILENAME_SIZE];
    char padding;
};

#endif
