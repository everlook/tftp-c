#ifndef UTILS_H
#define UTILS_H

#define MAX_PATH_SIZE 128

FILE *utils_read_file(char *root, int r_size, char *filename, int f_size);

#endif
