#ifndef INET_CORE_H
#define INET_CORE_H

int inetcore_new_socket(int type);

void inetcore_new_addr(struct sockaddr_in *addr, int port);

int inetcore_new_bind(int fd, struct sockaddr_in *addr);

#endif
