#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

#include "common.h"
#include "inetcore.h"
#include "tftp.h"
#include "utils.h"


int main(int argc, char **argv)
{
    int sfd;
    int running;
    struct sockaddr_in svr_addr;
    struct sockaddr_in cli_addr;
    socklen_t addrlen = sizeof(cli_addr);
    ssize_t num_read;
    ssize_t pkt_size;
    char *path;
    struct tftp_request_pkt req_pkt;
    FILE *file;

    /* we are using an fd_set in case the client dies during a transfer */
    int retval;
    int retry;
    fd_set rdfs;
    struct timeval tv;

    if (argc < 2) {
        printf("Usage: %s <path> \n", argv[0]);
        exit(EXIT_FAILURE);
    }

    path = argv[1];
    printf("Path: %s \n", path);

    if ((sfd = inetcore_new_socket(SOCK_DGRAM)) < 0) {
        perror("Cannot create socket \n");
        exit(EXIT_FAILURE);
    }

    inetcore_new_addr(&svr_addr, TFTP_PORT);

    if (inetcore_new_bind(sfd, &svr_addr) < 0) {
        perror("Cannot bind socket \n");
        exit(EXIT_FAILURE);
    }

    running = 1;
    memset(&req_pkt, 0, sizeof(struct tftp_request_pkt));

    printf("Waiting on client \n");
    num_read = recvfrom(sfd, &req_pkt, sizeof(struct tftp_request_pkt), 0
        , (struct sockaddr *)&cli_addr, &addrlen);
    printf("Received %d bytes, using port %d \n", (int)num_read, cli_addr.sin_port);

    file = utils_read_file(path, strlen(path), req_pkt.filename, 
        strlen(req_pkt.filename));

    if (file == NULL) {
        struct tftp_error_pkt err_pkt;
        memset(&err_pkt, 0, sizeof(struct tftp_error_pkt));
        
        err_pkt.optcode = htons(OPTCODE_ERROR); 

        if (sendto(sfd, &err_pkt, sizeof(struct tftp_error_pkt), 0, 
                    (struct sockaddr *)&cli_addr, addrlen) != 
            sizeof(struct tftp_error_pkt)) {
            perror("error packet send failed \n");
        }
    } else {
        struct tftp_data_pkt data_pkt;
        struct tftp_ack_pkt ack_pkt;
        int pkt_num = 1;

        /* add sfd to fd_set */
        FD_ZERO(&rdfs);
        FD_SET(sfd, &rdfs);
    

        /* add a retry count */
        retry = 3;

        memset(&data_pkt, 0, sizeof(struct tftp_data_pkt));
        memset(&ack_pkt, 0, sizeof(struct tftp_ack_pkt));

        while (running) {
            num_read = fread(&(data_pkt).data, 1, MAX_DATA_SIZE, file);

            if (num_read == 0) {
                break;
            }

            /* get payload size */
            pkt_size = num_read+4;
            data_pkt.optcode = htons(OPTCODE_DATA);
            data_pkt.block = htons(pkt_num);

            printf("sending block %d, pkt size %d \n", ntohs(data_pkt.block), 
                    (int)pkt_size);

            if (sendto(sfd, &data_pkt, pkt_size, 0, 
                        (struct sockaddr *)&cli_addr, addrlen) != 
                pkt_size) {
                perror("data packet send failed \n");
            }
        
            while(1) {
                /* wait for up to 2 seconds */
                tv.tv_sec = 2;
                tv.tv_usec = 0;

                /* use select because recvfrom is blocking */
                retval = select(sfd+1, &rdfs, NULL, NULL, &tv);
                if (retval == -1) {
                    printf("select error %d \n", errno);
                    exit(EXIT_FAILURE);
                } else if (retval <= 0) {
                    printf("timeout, retry %d \n", retry);
                    retry -= 1;
                    if (retry == 0) {
                        printf("Done waiting on client, extiting \n");
                        running = 0;
                        break;
                    }
                    continue;
                }
                /* reset retry count */
                retry = 3;

                num_read = recvfrom(sfd, &ack_pkt, sizeof(struct tftp_ack_pkt), 0
                    , (struct sockaddr *)&cli_addr, &addrlen);
                printf("received ack for block %d \n", ntohs(ack_pkt.block));
                pkt_num++;
                break;
            }
        }
    }

    if (file != NULL) {
        fclose(file);
    }
    if (sfd) {
        close(sfd);
    }

    return 0;
}
